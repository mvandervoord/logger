#include "unity.h"
#include "Executor.h"

#include "mock_stm32l4xx_hal.h"
#include "mock_stm32l4xx_hal_adc.h"

void setUp(void)
{
}

void tearDown(void)
{
}

void test_Executor_NeedToImplement(void)
{
    ADC_HandleTypeDef testdef;
    HAL_ADC_Start_ExpectAndReturn(&testdef, HAL_OK);

    Executor_Init();
}
