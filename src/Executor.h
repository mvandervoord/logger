#ifndef _EXECUTOR_H
#define _EXECUTOR_H

void Executor_Init(void);
void Executor_Exec(void);

#endif // _EXECUTOR_H
